# -*- coding: utf-8 -*-
"""
Created on Sun Nov 01 19:53:41 2023

@author: Magda
"""
import random

capital_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta'}

def select_one(capital_dict):
   key_list = list(capital_dict.keys())
   select_list=[]
   random_key = random.choice(key_list)
   select_val = capital_dict[random_key]
   select_list.append(random_key)
   select_list.append(select_val)
   return select_list

def ask_quastion(select_list):
    a= input(f"Podaj nazwe stolicy dla {select_list[0]}: ")
    while a!=select_list[1]:
        a= input ("Podaj  prawidłowa nazwę stolicy albo przerwij program /exit/: ")
        if a=="exit":
           print (f"Prawidłowa odpowiedź to: {select_list[1]}. Do zobaczenia")
           break
    if a==select_list[1]:
       print("Odpowiedź poprawna")       

def main():
    select_el=select_one(capital_dict)
    ask_quastion(select_el)

main()