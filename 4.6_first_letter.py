# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 16:39:50 2023

@author: Magda
"""
def make_upercase(word):
   first_letter_upper=word[0].upper()
   return first_letter_upper

def main():
    password=input("Tell me your password: ")
    letter=make_upercase(password)
    print("The first letter was:" + letter)

if __name__== "__main__":
		main()