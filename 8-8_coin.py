# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 13:43:46 2023

@author: Magda
"""
import random

def make_flip():
    a=int(3)
    if random.randint(0, 1) == 0:
        a=0
    else:
        a=1
    return a

def how_flip():
    flip_list=[]
    flip_list.append(make_flip())
    flip_list.append(make_flip())
    i=1
    while flip_list[i]==flip_list[i-1]:
        flip_list.append(make_flip())
        i=i+1
    #print(flip_list)
    return len(flip_list)

def cal_average(lista):
    ave=sum(lista)/len(lista)
    return ave

def main():
    ave_list=[]
    for i in range(10000):
      ave_list.append(how_flip())
    #print(len(ave_list))
    print(f"Srednia {cal_average(ave_list)}")
            
main()