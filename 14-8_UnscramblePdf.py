from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter


pdf_path = Path(r"E:\ZaRaczke\book\scrambled.pdf")
output_path = Path(r"E:\ZaRaczke\book\unscrambled.pdf")

pdf_reader = PdfReader(str(pdf_path))
pdf_writer = PdfWriter()
list_text = []
list_pages = list(pdf_reader.pages)

for page in list_pages:
    text = page.extractText()
    list_text.append(int(text)-1)

list_pages_sort = [list_pages[i] for i in list_text]

for page in list_pages_sort:
    rotation = page["/Rotate"]
    page.rotateCounterClockwise(rotation)
    pdf_writer.addPage(page)

with output_path.open(mode="wb") as output_file:
    pdf_writer.write(output_file)