# -*- coding: utf-8 -*-
"""
Created on Sat Nov 11 12:13:49 2023

@author: Magda
"""
import random

Nouns= ["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"] 
Verbs= ["kicks", "jingles", "bounces", "slurps", "meows", "explodes", "curdles"] 
Adjectives= ["furry", "balding", "incredulous", "fragrant", "exuberant", "glistening"] 
Prepositions= ["against", "after", "into", "beneath", "upon", "for", "in", "like", "over", "within"] 
Adverb= ["curiously", "extravagantly", "tantalizingly", "furiously", "sensuously"]

def select_word(word_list,n):   
    sel_word_list=[]
    for i in range (0,n):
       k= random.randint(0, len(word_list))
       sel_word_list.append(word_list[k-1])
    #print(sel_word_list)
    return sel_word_list

def make_sentance(noun_list, verb_list, adjective_list, preposition_list, adverb_list):
    if adjective_list[0][0] in ["a", "i", "e", "o", "u"]:
       sentence= print (f"An {adjective_list[0]} {noun_list[0]} {verb_list[0]} {preposition_list[0]} "\
              f"the {adjective_list[1]} {noun_list[1]} {adverb_list[0]} "\
              f"the  {noun_list[0]} {verb_list[1]} "\
              f"the {noun_list[1]} {verb_list[2]} {preposition_list[1]} "\
              f"a  {adjective_list[2]} {noun_list[2]}")
    else:
        sentence= print (f"A {adjective_list[0]} {noun_list[0]} {verb_list[0]} {preposition_list[0]} "\
               f"the {adjective_list[1]} {noun_list[1]} {adverb_list[0]} "\
               f"the  {noun_list[0]} {verb_list[1]} "\
               f"the {noun_list[1]} {verb_list[2]} {preposition_list[1]} "\
               f"a  {adjective_list[2]} {noun_list[2]}")
              
    return (sentence)
   
   
def main():
   noun_list=select_word(Nouns,3)
   verb_list=select_word(Verbs,3)
   adjective_list=select_word(Adjectives,3)
   preposition_list=select_word(Prepositions,2)
   adverb_list=select_word(Adverb,1)
   make_sentance(noun_list, verb_list, adjective_list, preposition_list, adverb_list)
 

   
main()