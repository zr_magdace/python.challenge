# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 20:42:25 2023

@author: Magda
"""
def convert_cel_to_far(cel):
   far=float(cel)*9/5+32
   return far

def convert_far_to_cel(far):
   cel=(float(far)-32)*5/9
   return cel

def main():
    cel=input("Enter a temperature in degrees C: ")
    far_cal=convert_cel_to_far(cel)
    print(f"{cel} C= {far_cal:.2f} F" )
    
    far=input("Enter a temperature in degrees F: ")
    cel_cal=convert_far_to_cel(far)
    print(f"{far} F= {cel_cal:.2f} C" )

if __name__== "__main__":
		main()

