# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 19:22:47 2023

@author: Magda
"""

universities = [
    ['California Institute of Technology', 2175, 37704],
    ['Harvard', 19627, 39849],
    ['Massachusetts Institute of Technology', 10566, 40732],
    ['Princeton', 7802, 37000],
    ['Rice', 5879, 35551],
    ['Stanford', 19535, 40569],
    ['Yale', 11701, 40500]
]

def enrollment_stats(uni_list):
    stud_list=[el[1] for el in uni_list]
    #print (enrollment_list)
    tuit_list=[el[2] for el in uni_list]
    return((stud_list, tuit_list))
    
def mean(b_list):
    mean_val=sum(b_list)/len(b_list)
    return(mean_val)

def median(b_list):
    print(b_list)
    l=len(b_list)
    b_list.sort()
    if l%2==0:
      median_val=(b_list[l-1]+b_list[l])/2
    else:
      median_val=(b_list[int(l/2)])
    return(median_val)

def main():
    stud=enrollment_stats(universities)[0]
    tot_stud=sum(stud)
    tuit=enrollment_stats(universities)[1]
    tot_tuit=sum(tuit)
    mean_stud=mean(stud)
    median_stud=median(stud)
    mean_tuit=mean(tuit)
    median_tuit=median(tuit)
    
    print (f"Total students:   {tot_stud}")
    print (f"Total tuition:  $ {tot_tuit}")
    print (f"Student mean:     {mean_stud:.2f}")
    print (f"Student median:   {median_stud}")
    print (f"Tuition mean:   $ {mean_tuit:.2f}")
    print (f"Tuition median: $ {median_tuit}")
#
main()