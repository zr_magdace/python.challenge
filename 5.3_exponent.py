# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 20:42:25 2023

@author: Magda
"""
def raise_power(base, exponent):
   c=float(base)**float(exponent)
   return c

def main():
    base=input("Enter a base: ")
    exponent=input("Enter an exponent: ")
    c=raise_power(base, exponent)
    print(f"{base} to the power of {exponent}= {c:.3f}" )

if __name__== "__main__":
		main()

