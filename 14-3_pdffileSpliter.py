from pathlib import Path
from PyPDF2 import PdfReader, PdfWriter

class PdfFileSplitter:
    def __init__(self, pdf_path):
        self.pdf = PdfReader(pdf_path)
        self.pdf_part_1 = PdfWriter()
        self.pdf_part_2 = PdfWriter()

    def split(self, breakpoint):
        for page in self.pdf.pages[:breakpoint]:
            self.pdf_part_1.addPage(page)
        for page in self.pdf.pages[breakpoint:]:
            self.pdf_part_2.addPage(page)

    def write(self, filename):
        with Path(filename + "part_1.pdf").open(mode="wb") as output_file_1:
            self.pdf_part_1.write(output_file_1)
        with Path(filename + "part_2.pdf").open(mode="wb") as output_file_2:
            self.pdf_part_2.write(output_file_2)

pdf_splitter = PdfFileSplitter(r"E:\ZaRaczke\book\tekst.pdf")
pdf_splitter.pdf
pdf_splitter.split(10)

pdf_splitter.write("tekst_")