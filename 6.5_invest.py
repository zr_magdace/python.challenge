# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 20:42:25 2023

@author: Magda
"""
def invest(amount, rate, years):
   for i in range(years):
       amount=amount*(rate/100)+amount
       print(f"year {i+1}: {amount:.2f} $")

def main():
    amount=float(input("Enter a amount: "))
    rate=float(input("Enter a rate in %: "))
    years=int(input("Enter a time in year: "))
    print(f"Result:")
    invest(amount,rate, years)
    
if __name__== "__main__":
	main()

