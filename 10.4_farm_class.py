#Farma zwierząt

class Person:
     counter_person=0
     def __init__(self, ID, name, surname, date_start):
        self.ID=ID
        self.name=name
        self.surname=surname
        self.date_start=date_start
        self.date_stop=00-00-0000
        Person.counter_person += 1       # odwołanie do pola statycznego
        self.counter_person = Person.counter_person    
        
     def __str__(self):   #odpowiedz na print(object)
       return f" Na farmie mieszka {Person.counter_person} osób"
                
     def speak_person  (self): 
            return f"{self.name} mówi cześć" 
        
     def move_person (self): #metoda służąca modyfikacji wartości pól
        new_ID_building=input ("Wprowadz ID budynku, nowego miejsca zamieszkania: ")
        self.ID_building=new_ID_building 
        
     def remove_person (self):   #metoda służąca modyfikacji wartości pól
        new_date_stop=input ("Wprowadz datę odejscia osoby z farmy: ")
        self.date_stop=new_date_stop                 
        Person.counter_person -= 1       # odwołanie do pola statycznego
        self.counter_person = Person.counter_person    

class Person_Owner(Person):    
    counter_person=0
    def __init__(self, ID, name, surname, date_start, ID_building ):
       super().__init__(ID, name, surname, date_start)
       self.ID_building=ID_building
       Person_Owner.counter_person += 1       # odwołanie do pola statycznego
       self.counter_person = Person_Owner.counter_person
       
    def __str__(self):   #odpowiedz na print(object)
      return f" Na farmie jest {Person_Owner.counter_person} współwłascicieli"   
       
    def create_person():
       ID = input ("wprowadź numer identyfikacyjny ID: ")        
       name = input ("Wprowadź imię: ")
       surname = input ("Wprowadź nazwisko: ")
       ID_building = input ("Wprowadź miejsce zamieszkania (ID budynku): ")
       date_start = input ("Wprowadź datę urodzenia: ")
       date_stop=00-00-0000
       return Person(ID, name, surname, ID_building, date_start, date_stop) 
           

class Building:
     counter_building=0
     def __init__(self, ID, XYZ, name, volume, area, number_floor, ID_owner, date_start):
             self.ID=ID
             self.XYZ=XYZ
             self.name=name
             self.volume=volume
             self.area=area
             self.number_floor=number_floor
             self.ID_owner=ID_owner
             self.date_start=date_start
             self.date_stop=00-00-0000
             Building.counter_building += 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building
        
     def __str__(self):
             return f"Na farmie jest {Building.counter_building}"  
     

     def kill_building (self):   #metoda służąca modyfikacji wartości pól
             new_date_stop=input ("Wprowadz datę zburzenia budynku: ")
             self.date_stop=new_date_stop                  
             Building.counter_building -= 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building   
       
class Building_cow(Building):
     counter_building=0  
     def __init__(self, ID, XYZ, name, volume, area, ID_owner, date_start, tank_vol, ID_workers):  
            super().__init__(ID, XYZ, name, volume, area, ID_owner, date_start)
            self.tank_vol= tank_vol
            self.ID_workers = ID_workers
            Building_cow.counter_building +=1
            self.counter_buildig= Building_cow.counter_building
                       
     def kill_building_cow(self):
            super().kill_building()
            Building_cow.counter_building -= 1       # odwołanie do pola statycznego
            self.counter_building = Building_cow.counter_building   
            return f"Na farmie  jest {Building.counter_building} budynków, w tym {Building_cow.counter_building} mleczarni"            
        
     def add_tank(self):
             new_tank_vol=input ("Wprowadz nowa pojemnosc zbiornika na mleko: ")
             self.tank_vol= new_tank_vol               
    
     def change_workers(self):
             new_ID_workers=input ("Wprowadz nową listę pracowników: ")
             self.ID_workers=new_ID_workers      
 
class Product:
    counter_product=0
    def __init__(self, ID, kind, serie, date_start, item):
            self.ID=ID
            self.kind=kind
            self.serie=serie
            self.date_start=date_start
            self.date_stop=00-00-0000
            self.items=0
            Product.counter_product += 1       # odwołanie do pola statycznego
            self.counter_product = Product.counter_product

      
    def stop_product(self, new_date_stop):     
        self.date_stop=new_date_stop                 
        Product.counter_product -= 1       # odwołanie do pola statycznego
        self.counter_product = Product.counter_product
        
    def add_items(self):
        new_items=input ("Dodaj nastepująca ilosc do danej partii produktu: ")
        self.items += new_items 
      
    def send_items(self, ID_building):
         return f"Produkt {self.ID} w iloci {self.item} został wysłany do budynku o ID {ID_building}"
     
class Animal:
    count_animal=0
    def __init__(self, ID, name, ID_building, date_birth):
            self.ID=ID
            self.name=name
            self.ID_building=ID_building
            self.date_birth=date_birth
            self.date_death=00-00-0000
            Animal.counter_animal += 1       # odwołanie do pola statycznego
            self.counter_animal = Animal.counter_animal
            
    def kill_animal(self):     
           new_date_death=input ("Wprowadz datę odejcia zwierzecia: ")
           self.date_death=new_date_death   
           Animal.counter_animal -= 1       # odwołanie do pola statycznego
           self.counter_animal = Animal.counter_animal
           return f"Na farmie  jest {Animal.counter_animal} zwierzat"  

        
class Cow(Animal):
    def __init__(self, ID, name, ID_diary, date_birth, date_death, ID_product):
        super().__init__(ID, name, ID_diary, date_birth, date_death)
        self.ID_product=ID_product
    
class Dog(Animal):
    def __init__(self, ID, name, ID_building, date_birth, date_death, ID_owner):
        super().__init__(ID, name, ID_building, date_birth, date_death)
        self.ID_owner=ID_owner

def main():
    farmer_1=Person(1,"Antoni","Kot", "01-12-2000")
    print(farmer_1)
    
    owner_1=Person_Owner(2,"Albert","Krolik", "01-12-2000", 1)
    print(owner_1)
    
    farmer_2=Person(3,"Alan","Zajac", "01-12-2000")
    print(farmer_2)
    
    owner_2=Person_Owner(4,"Alicja","Jaskołka", "01-12-2000", 3)
    print(owner_1)
    
    owner_1.remove_person()
    print(owner_1.date_stop)
    
   
main()
    

            
