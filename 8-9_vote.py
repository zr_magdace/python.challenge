# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 13:43:46 2023

@author: Magda
"""
import random

def make_vote(vote_list, n):
    for i in range (n):
        a=random.randint(0,100)
        if a> 50:
          vote_list.append(a)
        b=len(vote_list)/n*100  
    #print(vote_list)  
    return b


def main():
    n=10
    
    vote_list=[]
    result_list=[]
    for i in range(3):            
        result_list.append(make_vote(vote_list,n))
        vote_list=[]
        print(f"Kandydat A - Średnia dla regionu {i+1} to: {result_list[i]:.0f}")
        
    
  
main()