# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 21:47:48 2023

@author: Magda
"""
import pathlib

path_in= pathlib.Path(r"C:\Users\Magda\Documents\Magda_priv\ZR\document")
path_out= pathlib.Path(r"C:\Users\Magda\Documents\Magda_priv\ZR\image")

path_out.mkdir()

for path in path_in.rglob("*.*"):
    if path.suffix.lower() in [".png", ".jpg", ".gif"]:
        path.replace(path_out / path.name)