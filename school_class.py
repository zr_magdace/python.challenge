#SCHOOL
class School:
     counter_school=0
     def __init__(self, ID_school, voivodship, number, name, date_start):
        self.ID_school=ID_school
        self.voivodship=voivodship
        self.number=number
        self.name=name
        self.date_start=date_start
        self.date_stop=00-00-0000
        School.counter_school += 1       # odwołanie do pola statycznego
        self.counter_school = School.counter_school
                
     def print_data (self): 
            return f"Szkoła podstawowan numer {self.number} w wojewodztwie {self.voivodship} imienia {self.name}" 
        
     def stop_school (self): #metoda służąca modyfikacji wartości pól
        new_data_stop=input ("Wprowadz data likwidacji szkoly: ")
        self.data_stop=new_data_stop 


class Staff:
     counter_staff=0
     def __init__(self, ID_staff, name, surname, pesel, position, date_start, ID_school):
        self.ID_staff=ID_staff
        self.name=name
        self.surname=surname
        self.pesel=pesel
        self.position=position
        self.date_start=date_start
        self.ID_school=ID_school
        self.date_stop=00-00-0000
        Staff.counter_staff += 1       # odwołanie do pola statycznego
        self.counter_staff = Staff.counter_staff  
        
     def print_info (self):   
       return f" Osoba o ID {self.ID_staff} to {self.surname} {self.name}"
                
     def move_staff (self): 
        new_position=input ("Wprowadz nowe stanowisko: ")
        self.position=new_position
        
     def stop_stuff (self):  
        new_date_stop=input ("Wprowadz datę odejscia osoby ze szkoły: ")
        self.date_stop=new_date_stop                 
        Staff.counter_staff -= 1       
        self.counter_staff = Staff.counter_staff  

class Teacher(Staff):     
    def __init__(self, ID_staff, name, surname, pesel, possision, date_start, ID_school, ID_subject_list, ID_class_list):
       super().__init__(ID_staff, name, surname, pesel, possision, date_start, ID_school)
       self.ID_subject_list=ID_subject_list
       self.ID_class_list=ID_class_list
      
    def print_info_teacher(self):   #odpowiedz na print(object)
      return f" Nauczyciel o ID {self.ID_staff} uczy {self.ID_subject_list} w klasach {self.ID_class}"  

class Director(Teacher):     
    def __init__(self, ID_staff, name, surname, pesel, possision, date_start, ID_school, ID_subject_list, ID_class_list, ID_building_list):
       super().__init__(ID_staff, name, surname, pesel, possision, date_start, ID_school, ID_subject_list, ID_class_list)
       self.ID_building_list=ID_building_list
            
    def print_info_teacher(self):   #odpowiedz na print(object)
      return f" Dyrektor szkoły {self.ID_school} uczy {self.ID_subject} w klasach {self.ID_class}"        

class Class:
    counter_class=0
    def __init__(self, ID_class, name, date_start, room, ID_plan, ID_student_list):
        self.ID_class=ID_class
        self.name=name
        self.date_start=date_start
        self.room=room
        self.ID_plan=ID_plan
        self.ID_student_list=ID_student_list
        self.date_stop=00-00-0000
        Class.counter_class += 1       # odwołanie do pola statycznego
        self.counter_class = Class.counter_class    

class Plan:
    def __init__(self, ID_plan, ID_subject_list, ID_teacher_list, room_list):
        self.ID_plan=ID_plan
        self.time_list=[[i,j] for i in range (1,6) for j in range (1,10)] # [i,j]- dZien tygodnia, j - numer lekcji
        self.ID_subject_list=ID_subject_list
        self.ID_teacher_list
        self.room_list=room_list
     
    def change_list_subject(self):    
        new_ID_subject_list=input ("Wprowadz nowy harmonogram przedmiotow: ")
        self.ID_subject_list=new_ID_subject_list
        
    def change_list_teacher(self):    
       new_ID_teacher_list=input ("Wprowadz nowy harmonogram nauczycieli: ")
       self.ID_teacher_list=new_ID_teacher_list
       
    def change_list_room(self):    
       new_room_list=input ("Wprowadz nowy harmonogram sal: ")
       self.room_list=new_room_list   
       
       
class Student:
    counter_student=0
    def __init__(self, ID_student, name, surname, pesel, start_student, ID_class, ID_register):
        self.ID_student=ID_student
        self.name=name
        self.surname=surname
        self.pesel=pesel
        self.start_student=start_student
        self.stop_student=00-00-0000
        self.ID_class=ID_class
        self.ID_register=ID_register
        Student.counter_student += 1       # odwołanie do pola statycznego
        self.counter_student = Student.counter_student   
        
    def move_student (self): 
       new_ID_class=input ("Wprowadz nowy identyfikator klasy: ")
       self.ID_class=new_ID_class
       
    def stop_student (self):  
       new_date_stop=input ("Wprowadz datę odejscia ucznia ze szkoły: ")
       self.date_stop=new_date_stop                 
       Student.counter_student -= 1       
       self.counter_student = Student.counter_student   

class Register:
    def __init__(self, ID_register, time_list, grade_list, subject_list, description_list, ID_student):
        self.ID_register=ID_register
        self.time_list=time_list
        self.grade_list=grade_list
        self.subject_list=subject_list
        self.description_list=description_list
        self.ID_student=ID_student
        
    def change_list_time(self):    
        new_time_list=input ("Wprowadz nowy harmonogram przedmiotow: ")
        self.time_list=new_time_list
        
    def change_list_grade(self):    
       new_grade_list=input ("Wprowadz nowy harmonogram nauczycieli: ")
       self.grade_list=new_grade_list
       
    def change_list_subject(self):    
       new_subject_list=input ("Wprowadz nowy harmonogram sal: ")
       self.subject_list=new_subject_list   
       
    def change_list_decsription(self):    
       new_description_list=input ("Wprowadz nowy wykaz opisow: ")
       self.description_list=new_description_list      
       
            
class Building:
     counter_building=0
     def __init__(self, ID_building, city, street, volume, area, number_floor, ID_school, date_start):
             self.ID_building=ID_building
             self.city=city
             self.street=street
             self.volume=volume
             self.area=area
             self.number_floor=number_floor
             self.ID_school=ID_school
             self.date_start=date_start
             self.date_stop=00-00-0000
             Building.counter_building += 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building
        
     def print_info(self):
             return f"Szkoła składa się z {Building.counter_building} budynków"  
     
     def stop_building (self):   #metoda służąca modyfikacji wartości pól
             new_date_stop=input ("Wprowadz datę zburzenia budynku: ")
             self.date_stop=new_date_stop                  
             Building.counter_building -= 1       # odwołanie do pola statycznego
             self.counter_building = Building.counter_building   
       



    

            
