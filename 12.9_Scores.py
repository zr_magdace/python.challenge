# -*- coding: utf-8 -*-
"""
Created on Mon Jan  1 13:59:12 2024

@author: Magda
"""

import csv
from pathlib import Path
file_path=Path("C:/Users/Magda/Documents/Magda_priv/ZR/book_sumup/scores.csv")
file_path_out=Path("C:/Users/Magda/Documents/Magda_priv/ZR/book_sumup/scores__high.csv")
#ODCZYT z pliku  nagłowkiem (pierwsza wiersz to nagłówek), dane z intami
#i zapisanie jako lista słownikow (kazdy słownik to wiersz z pliku)
def process_row(row):
    row["score"] = float(row["score"])
    return row

scores=[]
with file_path.open(mode="r", encoding="utf-8") as file:  
    #reader=csv.reader(file)      #wynik to lista list
    reader =csv.DictReader( file) # wynik to lista słownikow
    for row in reader:
        scores.append(row)

print(scores)
#Transoframcja listy słownikow na słownik z wybranymi danymi        
high_scores={}
for item in scores:
    name=item["name"]
    score=item["score"]
    
    if name not in high_scores:
       high_scores[name]=score
    
    else:
        if score >high_scores[name]:
          high_scores[name]=score
          
print(high_scores)
    
# ZAPISYWANIE słownika do pliku / key zapisuje sie jako jedna kolumna, value - jako druga kolumn

with file_path_out.open(mode="a", encoding="utf-8", newline="") as file:
    writer=csv.DictWriter(file, fieldnames=["name", "high_score"])
    writer.writeheader()
    for key in high_scores:
        row_high_scores={"name": key, "high_score":high_scores[name]}  #tworze liste jedoelementowa
        writer.writerow(row_high_scores)