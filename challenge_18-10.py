import tkinter as tk
from tkinter import filedialog
import random

class GuiPoem():
    def __init__(self, logic, window):
        self.logic = logic
        self.logic.gui = self
        self.window = window
        self.logic.nouns = []
        self.logic.verbs = []
        self.logic.adjectives = []
        self.logic.adverbs = []
        self.logic.prepositions = []

        self.header_frame = tk.Frame(master=self.window)
        self.header_label = tk.Label(master=self.header_frame)
        self.header_label["text"] = (
            "Enter your favorite words: 3 nouns, 3 verbs, 3 adjectives, 3 prepositions, 1 adverb."
        )
        self.header_label.pack()
        self.header_frame.pack(padx=5, pady=5)

        self.input_frame = tk.Frame(master=self.window)
        self.label_noun = tk.Label(self.input_frame, text="Nouns:")
        self.label_verb = tk.Label(self.input_frame, text="Verbs:")
        self.label_adj = tk.Label(self.input_frame, text="Adjectives:")
        self.label_prep = tk.Label(self.input_frame, text="Prepositions:")
        self.label_adv = tk.Label(self.input_frame, text="Adverbs:")

        self.entry_noun = tk.Entry(self.input_frame, width=80)
        self.entry_verb = tk.Entry(self.input_frame, width=80)
        self.entry_adj = tk.Entry(self.input_frame, width=80)
        self.entry_prep = tk.Entry(self.input_frame, width=80)
        self.entry_adv = tk.Entry(self.input_frame, width=80)

        self.label_noun.grid(row=2, column=1, sticky=tk.E)
        self.entry_noun.grid(row=2, column=2)
        self.label_verb.grid(row=3, column=1, sticky=tk.E)
        self.entry_verb.grid(row=3, column=2)
        self.label_adj.grid(row=4, column=1, sticky=tk.E)
        self.entry_adj.grid(row=4, column=2)
        self.label_prep.grid(row=5, column=1, sticky=tk.E)
        self.entry_prep.grid(row=5, column=2)
        self.label_adv.grid(row=6, column=1, sticky=tk.E)
        self.entry_adv.grid(row=6, column=2)
        self.input_frame.pack(padx=5, pady=5)

        self.generate_frame = tk.Frame(master=self.window)
        self.generate_frame.pack(pady=10)
        self.generate_button = tk.Button(self.generate_frame, command=self.logic.generate_poem, text="Generate")
        self.generate_button.pack()

        self.result_frame = tk.Frame(master=self.window)
        self.result_frame["relief"] = tk.GROOVE
        self.result_frame["borderwidth"] = 5
        self.result_label = tk.Label(master=self.result_frame)
        self.result_label["text"] = "Your poem:"
        self.result_label.pack(pady=10)
        self.result_poem = tk.Label(self.result_frame)
        self.result_poem["text"] = "Press the 'Generate' button to display your poem."
        self.result_poem.pack(padx=5)
        self.save_button = tk.Button(self.result_frame, command=self.logic.save_poem_to_file, text="Save to file")
        self.save_button.pack(pady=10)
        self.result_frame.pack(fill=tk.X, padx=5, pady=5)

    def get_entry(self):
        self.logic.nouns = self.entry_noun.get().split(",")
        self.logic.verbs = self.entry_verb.get().split(",")
        self.logic.adjectives = self.entry_adj.get().split(",")
        self.logic.adverbs = self.entry_adv.get().split(",")
        self.logic.prepositions = self.entry_prep.get().split(",")
        #print(self.logic.nouns)

class LogicPoem():
    def __init__(self):
        self.nouns = []
        self.verbs = []
        self.adjectives = []
        self.adverbs = []
        self.prepositions = []
        self.result_poem = {}

    def check_unique_of_word(self, words):
        return list(set(words))

    def check_len_of_word_list(self):
        return (
            len(self.nouns) >= 3
            and len(self.verbs) >= 3
            and len(self.adjectives) >= 3
            and len(self.prepositions) >= 2
            and len(self.adverbs) >= 1
        )

    def write_poem(self):
        self.nouns = self.check_unique_of_word(self.nouns)
        self.verbs = self.check_unique_of_word(self.verbs)
        self.adjectives = self.check_unique_of_word(self.adjectives)
        self.prepositions = self.check_unique_of_word(self.prepositions)
        self.adverbs = self.check_unique_of_word(self.adverbs)

        if self.check_len_of_word_list():
            noun1 = random.choice(self.nouns)
            noun2 = random.choice(self.nouns)
            noun3 = random.choice(self.nouns)

            while noun1 == noun2:
                noun2 = random.choice(self.nouns)
            while noun1 == noun3 or noun2 == noun3:
                noun3 = random.choice(self.nouns)

            verb1 = random.choice(self.verbs)
            verb2 = random.choice(self.verbs)
            verb3 = random.choice(self.verbs)

            while verb1 == verb2:
                verb2 = random.choice(self.verbs)
            while verb1 == verb3 or verb2 == verb3:
                verb3 = random.choice(self.verbs)

            adj1 = random.choice(self.adjectives)
            adj2 = random.choice(self.adjectives)
            adj3 = random.choice(self.adjectives)

            while adj1 == adj2:
                adj2 = random.choice(self.adjectives)
            while adj1 == adj3 or adj2 == adj3:
                adj3 = random.choice(self.adjectives)

            prep1 = random.choice(self.prepositions)
            prep2 = random.choice(self.prepositions)
            while prep1 == prep2:
                prep2 = random.choice(self.prepositions)

            adv1 = random.choice(self.adverbs)

            if adj1[0] in "aeiou":
                art1 = "An"
            else:
                art1 = "A"

            if adj3[0] in "aeiou":
                art3 = "An"
            else:
                art3 = "A"

            poem = f"{art1} {adj1} {noun1}\n\n"
            poem = (
                poem + f"{art1} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
            )
            poem = poem + f"{adv1}, the {noun1} {verb2}\n"
            poem = poem + f"the {noun2} {verb3} {prep2} {art3} {adj3} {noun3}"

            self.result_poem["text"] = poem
        else:
            self.result_poem["text"] = "There was a problem with your words!\n"

    def save_poem_to_file(self):
        type_list = [("Text files", "*.txt")]
        file_name = filedialog.asksaveasfilename(
            filetypes=type_list, defaultextension="*.txt"
        )

        if file_name != "":
            with open(file_name, "w") as output_file:
                output_file.writelines(self.result_poem["text"])

    def generate_poem(self):
        self.gui.get_entry()
        self.write_poem()

def main():
    window = tk.Tk()
    window.title("Make your own poem!")
    logic = LogicPoem()
    gui = GuiPoem(logic, window)
    window.mainloop()


main()