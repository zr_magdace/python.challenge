import easygui as gui
from PyPDF2 import PdfReader, PdfWriter
#Wybor pliku .pdf
input_pdf_path = gui.fileopenbox("", "Select a PDF file", "*.pdf")
if input_pdf_path is None:
    exit()
#Wybor poczatowej strony
start_page = gui.enterbox("Enter the first page:", "Choice")
if start_page is None:
    exit()

# Walidacja wyboru
input_pdf = PdfReader(input_pdf_path)
total_pages = len(input_pdf.pages)
while (not start_page.isdigit() or int(start_page) < 0 or int(start_page) > total_pages or start_page is None):
    gui.msgbox("Provide a valid page number.", "Warn")
    start_page = gui.enterbox(" Enter the first page: ", "Choice")
    exit()

# Wybór ostatniej strony
end_page = gui.enterbox(" Enter the last page: ", "Choice")
if end_page is None:
    exit()

# Walidacja wyboru
while (not end_page.isdigit() or int(end_page) < int(start_page) or int(end_page) > total_pages or end_page is None):
    gui.msgbox("Provide a valid page number.", "Warn")
    end_page = gui.enterbox(
        "Enter the last page: ", "Choice"
    )
    exit()

# Wybor ściezki zapisu  nowego pliku .pdf
output_pdf_path = gui.filesavebox("", "Save the new PDF as...", "*.pdf")
if output_pdf_path is None:
    exit()

# Walidacja zapisu
while input_pdf_path == output_pdf_path:
    gui.msgbox("Please choose another file...", "Warn"
    )
    output_pdf_path = gui.filesavebox(
        "", "Save the new PDF as...", "*.pdf"
    )
    if output_pdf_path is None:
        exit()

# Nowy plik .pdf
output_PDF = PdfWriter()
for i in range(int(start_page) - 1, int(end_page)):
    page = input_pdf.pages[i]
    output_PDF.add_page(page)

with open(output_pdf_path, "wb") as output_file:
    output_PDF.write(output_file)